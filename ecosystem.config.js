module.exports = {
  apps: [
    {
      name: 'backend',
      script: 'node_modules/serve/bin/serve.js',
      args: ['-s', 'webapp', '-p', '3000'],
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ]
};
