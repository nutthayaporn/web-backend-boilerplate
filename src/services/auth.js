import { fetchApi } from "../utils";

const loginApi = (uid, password) => {
  const data = JSON.stringify({ uid, password });
  const headers = {
    "Content-Type": "application/json"
  };
  return fetchApi("auth/login", "POST", data, headers);
};

const getAuthByTokenApi = token => {
  const headers = {
    Authorization: `Bearer ${token}`
  };
  return fetchApi("auth/me", "GET", {}, headers);
};

export { loginApi, getAuthByTokenApi };
