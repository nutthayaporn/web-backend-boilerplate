import _ from "lodash";
import { fetchApi } from "../utils";

const getsettingsApi = () => {
  return fetchApi("settings").then(res => _.get(res, "data"));
};

const savesettingsApi = ({ name, value }) => {
  return fetchApi("settings", "POST", JSON.stringify({ name, value }));
};

export { getsettingsApi, savesettingsApi };
