import { fetchApi } from "../utils";

const importApi = (file, table) => {
  const data = new FormData();
  data.append("file", file);
  data.append("table", table);
  return fetchApi("import", "POST", data, false);
};

export { importApi };
