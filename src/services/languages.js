import _ from "lodash";
import { fetchApi, toObject } from "../utils";

const getlanguagesApi = () => {
  return fetchApi("languages")
    .then(res => _.get(res, "data"))
    .then(toObject);
};

const getlanguagesByIdApi = id => {
  return fetchApi(`languages/${id}`);
};

const createlanguagesApi = data => {
  return fetchApi("languages", "POST", JSON.stringify(data));
};

const updatelanguagesApi = (id, data) => {
  return fetchApi(`languages/${id}`, "PUT", JSON.stringify(data));
};

const deletelanguagesApi = id => {
  return fetchApi(`languages/${id}`, "DELETE");
};

export {
  getlanguagesApi,
  getlanguagesByIdApi,
  createlanguagesApi,
  updatelanguagesApi,
  deletelanguagesApi
};
