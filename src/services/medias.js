import _ from "lodash";
import { fetchApi, toObject } from "../utils";

const getmediasApi = () => {
  return fetchApi("medias")
    .then(res => _.get(res, "data"))
    .then(toObject);
};

const getmediasByIdApi = id => {
  return fetchApi(`medias/${id}`);
};

const createmediasApi = file => {
  const data = new FormData();
  data.append("file", file);
  return fetchApi("medias", "POST", data, false);
};

const updatemediasApi = (id, data) => {
  return fetchApi(`medias/${id}`, "PUT", JSON.stringify(data));
};

const deletemediasApi = id => {
  return fetchApi(`medias/${id}`, "DELETE");
};

export { getmediasApi, getmediasByIdApi, createmediasApi, updatemediasApi, deletemediasApi };
