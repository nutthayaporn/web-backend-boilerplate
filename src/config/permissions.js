export default [
  {
    title: "All",
    key: "all",
    children: [
      {
        title: "Page",
        key: "page",
        children: [
          {
            title: "Home",
            key: "page.home",
            children: [
              { title: "View", key: "page.home.view" },
              { title: "Save", key: "page.home.save" }
            ]
          }
        ]
      },
      {
        title: "Post",
        key: "post",
        children: [
          {
            title: "Categories",
            key: "post.categories",
            children: [
              { title: "View", key: "post.categories.view" },
              { title: "Add", key: "post.categories.add" },
              { title: "Edit", key: "post.categories.edit" },
              { title: "Delete", key: "post.categories.delete" }
            ]
          },
          {
            title: "Posts",
            key: "post.posts",
            children: [
              { title: "View", key: "post.posts.view" },
              { title: "Add", key: "post.posts.add" },
              { title: "Edit", key: "post.posts.edit" },
              { title: "Delete", key: "post.posts.delete" }
            ]
          }
        ]
      },
      {
        title: "User",
        key: "user",
        children: [
          {
            title: "Users",
            key: "user.users",
            children: [
              { title: "View", key: "user.users.view" },
              { title: "Add", key: "user.users.add" },
              { title: "Edit", key: "user.users.edit" },
              { title: "Delete", key: "user.users.delete" }
            ]
          },
          {
            title: "Roles",
            key: "user.roles",
            children: [
              { title: "View", key: "user.roles.view" },
              { title: "Add", key: "user.roles.add" },
              { title: "Edit", key: "user.roles.edit" },
              { title: "Delete", key: "user.roles.delete" }
            ]
          }
        ]
      },
      {
        title: "Setting",
        key: "setting",
        children: [
          {
            title: "Language",
            key: "setting.language",
            children: [
              { title: "View", key: "setting.language.view" },
              { title: "Add", key: "setting.language.add" },
              { title: "Edit", key: "setting.language.edit" },
              { title: "Delete", key: "setting.language.delete" }
            ]
          }
        ]
      }
    ]
  }
];
