import React from "react";
import _ from "lodash";
import { Root, NotFound, Dashboard, Setting, User } from "../pages";
import { Layout } from "../components";

export default [
  {
    component: Root,
    routes: [
      {
        path: "/",
        exact: true,
        component: Dashboard
      },
      {
        path: "/page/home",
        permissionPrefix: "page.home",
        permissions: ["all", "page.home.view", "page.home.save"],
        exact: true,
        component: props => (
          <Layout.Page
            {...props}
            pageName="Home"
            resource="page/home"
            formItems={[
              {
                field: "topic",
                label: "Topic",
                inputType: "text"
              },
              {
                field: "description",
                label: "Description",
                inputType: "wysiwyg"
              }
            ]}
          />
        )
      },
      {
        path: "/post/categories",
        permissionPrefix: "post.categories",
        permissions: [
          "all",
          "post.categories.view",
          "post.categories.add",
          "post.categories.edit",
          "post.categories.delete"
        ],
        exact: true,
        component: props => (
          <Layout.List
            {...props}
            pageName="Categories"
            filterKey={["name"]}
            resource="post_categories"
            tableDisplay={[{ key: "name", title: "Category" }, { key: "slug", title: "Slug" }]}
          />
        )
      },
      {
        path: "/post/categories/save/:id",
        permissionPrefix: "post.categories",
        permissions: [
          "all",
          "post.categories.view",
          "post.categories.add",
          "post.categories.edit",
          "post.categories.delete"
        ],
        exact: true,
        component: props => (
          <Layout.Save
            {...props}
            pageName="Category"
            redirectPath="/post/categories"
            resource="post_categories"
            formItems={[
              {
                field: "name",
                label: "Category",
                inputType: "text",
                fieldDecorator: {
                  rules: [{ required: true, message: "Please input category!" }]
                },
                onChange: (props, value) => {
                  if (props.saveType !== "update") {
                    props.form.setFieldsValue({
                      slug: _.snakeCase(value)
                    });
                  }
                }
              },
              {
                field: "slug",
                label: "Slug",
                inputType: "text",
                notTranslate: true,
                disabled: true
              }
            ]}
          />
        )
      },
      {
        path: "/posts",
        permissionPrefix: "post.posts",
        permissions: [
          "all",
          "post.posts.view",
          "post.posts.add",
          "post.posts.edit",
          "post.posts.delete"
        ],
        exact: true,
        component: props => (
          <Layout.List
            {...props}
            pageName="Posts"
            filterKey={["title"]}
            resource="posts"
            otherResources={["post_categories"]}
            tableDisplay={[
              { key: "title", title: "Title" },
              { key: "category.name", title: "Category" }
            ]}
            filters={[
              {
                key: "category_id",
                resource: "post_categories",
                type: "select",
                mapResource: {
                  id: "id",
                  label: "name"
                },
                placeholder: "Category"
              }
            ]}
          />
        )
      },
      {
        path: "/posts/save/:id",
        permissionPrefix: "post.posts",
        permissions: [
          "all",
          "post.posts.view",
          "post.posts.add",
          "post.posts.edit",
          "post.posts.delete"
        ],
        exact: true,
        component: props => (
          <Layout.Save
            {...props}
            pageName="Post"
            redirectPath="/posts"
            resource="posts"
            otherResources={["post_categories"]}
            formItems={[
              {
                field: "category_id",
                label: "Category",
                inputType: "select",
                relation: true,
                resource: "post_categories",
                mapResource: {
                  id: "id",
                  label: "name"
                }
              },
              {
                field: "title",
                label: "Title",
                inputType: "text",
                fieldDecorator: {
                  rules: [{ required: true, message: "Please input title!" }]
                }
              },
              {
                field: "description",
                label: "Description",
                inputType: "wysiwyg"
              },
              {
                field: "image_id",
                label: "Image",
                inputType: "image",
                notTranslate: true
              }
            ]}
          />
        )
      },
      {
        path: "/users",
        permissionPrefix: "user.users",
        permissions: [
          "all",
          "user.users.view",
          "user.users.add",
          "user.users.edit",
          "user.users.delete"
        ],
        exact: true,
        component: props => (
          <Layout.List
            {...props}
            pageName="Users"
            filterKey={["username", "email"]}
            resource="users"
            notTranslate={true}
            tableDisplay={[
              { key: "username", title: "Username" },
              { key: "email", title: "Email" }
            ]}
          />
        )
      },
      {
        path: "/users/save/:id",
        permissionPrefix: "user.users",
        permissions: [
          "all",
          "user.users.view",
          "user.users.add",
          "user.users.edit",
          "user.users.delete"
        ],
        exact: true,
        component: props => (
          <Layout.Save
            {...props}
            pageName="User"
            redirectPath="/users"
            resource="users"
            notTranslate={true}
            otherResources={["roles"]}
            formItems={[
              {
                field: "roles",
                label: "Roles",
                inputType: "select",
                relation: true,
                resource: "roles",
                mode: "multiple",
                mapResource: {
                  id: "id",
                  label: "name"
                }
              },
              {
                field: "username",
                label: "Username",
                inputType: "text",
                fieldDecorator: {
                  rules: [
                    {
                      required: true,
                      message: "Please input username!"
                    }
                  ]
                }
              },
              {
                field: "email",
                label: "Email",
                inputType: "text",
                fieldDecorator: {
                  rules: [
                    {
                      type: "email",
                      message: "The input is not valid email!"
                    },
                    {
                      required: true,
                      message: "Please input email!"
                    }
                  ]
                }
              },
              {
                field: "password",
                label: "Password",
                inputType: "password"
              }
            ]}
          />
        )
      },
      {
        path: "/user/roles",
        permissionPrefix: "user.roles",
        permissions: [
          "all",
          "user.roles.view",
          "user.roles.add",
          "user.roles.edit",
          "user.roles.delete"
        ],
        exact: true,
        component: props => (
          <Layout.List
            {...props}
            pageName="Roles"
            filterKey={["name", "slug", "description"]}
            resource="roles"
            notTranslate={true}
            tableDisplay={[
              { key: "name", title: "Role" },
              { key: "slug", title: "Slug" },
              { key: "description", title: "Description" }
            ]}
          />
        )
      },
      {
        path: "/user/roles/save/:id",
        permissionPrefix: "user.roles",
        permissions: [
          "all",
          "user.roles.view",
          "user.roles.add",
          "user.roles.edit",
          "user.roles.delete"
        ],
        exact: true,
        component: props => (
          <User.Roles.Save
            {...props}
            pageName="Role"
            redirectPath="/user/roles"
            resource="roles"
            notTranslate={true}
          />
        )
      },
      {
        path: "/setting/language",
        permissionPrefix: "setting.language",
        permissions: [
          "all",
          "setting.language.view",
          "setting.language.add",
          "setting.language.edit",
          "setting.language.delete"
        ],
        exact: true,
        component: Setting.Language.List
      },
      {
        component: NotFound
      }
    ]
  }
];
