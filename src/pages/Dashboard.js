import React, { Component } from "react";

export default class PageDashboard extends Component {
  render() {
    return (
      <div className="page-dashboard">
        <h1>Welcome to {process.env.REACT_APP_WEBSITE_NAME}</h1>
      </div>
    );
  }
}
