import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Layout, Menu, Icon, Dropdown, Avatar } from "antd";
import _ from "lodash";
import { renderRoutes } from "react-router-config";
import { checkLogin } from "../hoc";
import { Logo, LanguageSwitcher } from "../components";
import { initApp } from "../actions";
import { switchLanguage } from "../actions/languages";
import { logoutAction } from "../actions/auth";
import { authHasPermission } from "../utils";
import queryString from "../utils/queryString";
import "../assets/css/index.less";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const extendAction = {
  initApp,
  switchLanguage,
  logoutAction
};

@checkLogin(extendAction)
export default class Root extends Component {
  componentWillMount() {
    this.init();
  }
  componentWillReceiveProps(nextProps) {
    const lang = _.get(queryString.parse(this.props.location.search), "lang");
    const nextLang = _.get(queryString.parse(nextProps.location.search), "lang");
    if (!_.isEqual(lang, nextLang)) {
      this.checkLanguageParam(nextProps);
    }
  }
  init = async () => {
    await this.props.actions.initApp();
    this.checkLanguageParam(this.props);
  };
  checkLanguageParam = props => {
    const { actions, location } = props;
    const languages = _.get(this.props, "languages.data");
    const currentLanguage = _.get(this.props, "languages.currentActive");
    const parsed = queryString.parse(location.search);
    const langParam = _.get(parsed, "lang");
    if (!langParam) return false;
    if (langParam === currentLanguage) return false;
    const languagesArr = _.map(languages, language => language.code);
    if (!_.includes(languagesArr, currentLanguage)) return false;
    actions.switchLanguage(langParam);
    return true;
  };
  handleClickLogout = () => {
    this.props.actions.logoutAction();
  };
  render() {
    const authData = _.get(this.props, "auth.data");
    const permissions = _.get(authData, "permissions");
    const { route } = this.props;
    return (
      <Layout>
        <Sider>
          <div className="logo">
            <Logo color="white" />
          </div>
          <Menu theme="dark" mode="inline">
            {authHasPermission(permissions, ["page.home.view", "page.home.save"]) && (
              <SubMenu
                key="page"
                title={
                  <span>
                    <Icon type="home" theme="outlined" />
                    <span>Page</span>
                  </span>
                }
              >
                {authHasPermission(permissions, ["page.home.view", "page.home.save"]) && (
                  <Menu.Item key="page-home">
                    <NavLink to="/page/home">Home</NavLink>
                  </Menu.Item>
                )}
              </SubMenu>
            )}
            {authHasPermission(permissions, [
              "post.categories.view",
              "post.categories.add",
              "post.categories.edit",
              "post.categories.delete",
              "post.posts.view",
              "post.posts.add",
              "post.posts.edit",
              "post.posts.delete"
            ]) && (
              <SubMenu
                key="post"
                title={
                  <span>
                    <Icon type="tag" theme="outlined" />
                    <span>Post</span>
                  </span>
                }
              >
                {authHasPermission(permissions, [
                  "post.categories.view",
                  "post.categories.add",
                  "post.categories.edit",
                  "post.categories.delete"
                ]) && (
                  <Menu.Item key="post-categories">
                    <NavLink to="/post/categories">Categories</NavLink>
                  </Menu.Item>
                )}
                {authHasPermission(permissions, [
                  "post.posts.view",
                  "post.posts.add",
                  "post.posts.edit",
                  "post.posts.delete"
                ]) && (
                  <Menu.Item key="post-posts">
                    <NavLink to="/posts">Posts</NavLink>
                  </Menu.Item>
                )}
              </SubMenu>
            )}
            {authHasPermission(permissions, [
              "user.users.view",
              "user.users.add",
              "user.users.edit",
              "user.users.delete",
              "user.roles.view",
              "user.roles.add",
              "user.roles.edit",
              "user.roles.delete"
            ]) && (
              <SubMenu
                key="user"
                title={
                  <span>
                    <Icon type="user" theme="outlined" />
                    <span>User</span>
                  </span>
                }
              >
                {authHasPermission(permissions, [
                  "user.users.view",
                  "user.users.add",
                  "user.users.edit",
                  "user.users.delete"
                ]) && (
                  <Menu.Item key="user-users">
                    <NavLink to="/users">Users</NavLink>
                  </Menu.Item>
                )}
                {authHasPermission(permissions, [
                  "user.roles.view",
                  "user.roles.add",
                  "user.roles.edit",
                  "user.roles.delete"
                ]) && (
                  <Menu.Item key="user-roles">
                    <NavLink to="/user/roles">Roles</NavLink>
                  </Menu.Item>
                )}
              </SubMenu>
            )}
            {authHasPermission(permissions, ["setting.language.view", "setting.language.save"]) && (
              <SubMenu
                key="setting"
                title={
                  <span>
                    <Icon type="setting" theme="outlined" />
                    <span>Setting</span>
                  </span>
                }
              >
                {authHasPermission(permissions, [
                  "setting.language.view",
                  "setting.language.add",
                  "setting.language.edit",
                  "setting.language.delete"
                ]) && (
                  <Menu.Item key="setting-language">
                    <NavLink to="/setting/language">Language</NavLink>
                  </Menu.Item>
                )}
              </SubMenu>
            )}
          </Menu>
        </Sider>
        <Layout style={{ marginLeft: 200 }}>
          <Header>
            <ul className="menu menu-left" />
            <ul className="menu menu-right">
              <li>
                <LanguageSwitcher />
              </li>
              <li>
                <Dropdown
                  overlay={
                    <Menu>
                      <Menu.Item key="logout" onClick={this.handleClickLogout}>
                        Logout
                      </Menu.Item>
                    </Menu>
                  }
                  trigger={["click"]}
                >
                  <a className="ant-dropdown-link user-avatar">
                    {_.get(authData, "username")} <Avatar icon="user" /> <Icon type="down" />
                  </a>
                </Dropdown>
              </li>
            </ul>
          </Header>
          <Content>
            <div className="main-content">{renderRoutes(route.routes)}</div>
          </Content>
          <Footer>{process.env.REACT_APP_WEBSITE_NAME} ©2018</Footer>
        </Layout>
      </Layout>
    );
  }
}
