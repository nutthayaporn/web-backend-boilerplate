import React, { Component } from "react";
import { Form, Button, Input, message, Switch } from "antd";
import _ from "lodash";
import { withRedux } from "../../../hoc";
import { createlanguagesApi, updatelanguagesApi } from "../../../services/languages";
import { authHasPermission } from "../../../utils";

const FormItem = Form.Item;

const mapStateToProps = state => {
  return state;
};

const actionToProps = {};

@Form.create()
@withRedux(mapStateToProps, actionToProps)
export default class PageSettingsLanguagesSave extends Component {
  state = {
    submitting: false
  };
  create = async data => {
    const createdLanguage = await createlanguagesApi(data);
    if (_.get(createdLanguage, "status") === "success") {
      message.success("Create success.");
      return true;
    } else {
      message.error(_.get(createdLanguage, "message", "Create failed."));
      return false;
    }
  };
  update = async data => {
    const updatedLanguage = await updatelanguagesApi(this.props.data.id, data);
    if (_.get(updatedLanguage, "status") === "success") {
      message.success("Update success.");
      return true;
    } else {
      message.error(_.get(updatedLanguage, "message", "Update failed."));
      return false;
    }
  };
  submit = async data => {
    if (this.state.submitting) return false;
    this.setState({ submitting: true });
    let saveSuccess = false;
    if (!_.isEmpty(this.props.data)) {
      saveSuccess = await this.update(data);
    } else {
      saveSuccess = await this.create(data);
    }
    if (saveSuccess) this.props.onSave();
    this.setState({ submitting: false });
    return true;
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const data = {
          ...values
        };
        this.submit(data);
      }
    });
  };
  render() {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { data } = this.props;
    const { submitting } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="page-settings-languages save">
        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Code">
            {getFieldDecorator("code", {
              rules: [{ required: true, message: "Please input code!" }],
              initialValue: _.get(data, "code")
            })(<Input />)}
          </FormItem>
          <FormItem label="Name">
            {getFieldDecorator("name", {
              rules: [{ required: true, message: "Please input name!" }],
              initialValue: _.get(data, "name")
            })(<Input />)}
          </FormItem>
          <FormItem
            label="Icon"
            extra={
              <a href="http://flag-icon-css.lip.is" target="_blank" rel="noopener noreferrer">
                http://flag-icon-css.lip.is
              </a>
            }
          >
            {getFieldDecorator("icon", {
              initialValue: _.get(data, "icon")
            })(<Input />)}
          </FormItem>
          <FormItem label="Status">
            {getFieldDecorator("enabled", {
              initialValue: _.get(data, "enabled", true),
              valuePropName: "checked"
            })(<Switch />)}
          </FormItem>
          <FormItem style={{ textAlign: "right" }}>
            <Button
              type="primary"
              htmlType="submit"
              loading={submitting}
              disabled={!authHasPermission(permissions, [`${permissionPrefix}.edit`])}
            >
              {submitting ? "Saving..." : "Save"}
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}
