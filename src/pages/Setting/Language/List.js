import React, { Component } from "react";
import { Icon, Checkbox, message, Popconfirm, Button, Modal, Select } from "antd";
import _ from "lodash";
import SaveForm from "./Save";
import { HeaderPage, ModulePublish, ModuleBlock } from "../../../components";
import { getLanguagesAction } from "../../../actions/languages";
import { getSettingsAction } from "../../../actions/settings";
import { updatelanguagesApi, deletelanguagesApi } from "../../../services/languages";
import { savesettingsApi } from "../../../services/settings";
import { withRedux } from "../../../hoc";
import { authHasPermission } from "../../../utils";

const Option = Select.Option;

const getValue = languages => {
  return _.chain(languages)
    .filter(language => language.enabled)
    .map(language => language.id)
    .value();
};

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  getLanguagesAction,
  getSettingsAction
};

@withRedux(mapStateToProps, actionToProps)
export default class PageSettingsLanguagesList extends Component {
  state = {
    data: {},
    showSaveModal: false,
    prevLanguages: {},
    checkedValues: [],
    defaultLanguage: null
  };
  static getDerivedStateFromProps(props, state) {
    if (!_.isEqual(_.get(props, "languages"), state.prevLanguages)) {
      return {
        prevLanguages: props.languages,
        checkedValues: getValue(props.languages.data),
        defaultLanguage: props.settings.data.defaultLanguage
      };
    }
    return null;
  }
  handleDeleteLanguage = async id => {
    const deleteResult = await deletelanguagesApi(id);
    if (_.get(deleteResult, "status") === "success") {
      message.success("Delete success.");
      this.props.actions.getLanguagesAction();
    } else {
      message.error(_.get(deleteResult, "message", "Delete failed."));
    }
  };
  handleClickCreate = () => {
    this.setState({ data: {}, showSaveModal: true });
  };
  handleCancelModalCreate = () => {
    this.setState({ data: {}, showSaveModal: false });
  };
  handleEditLanguage = data => {
    this.setState({ data, showSaveModal: true });
  };
  handleSaveLanguage = () => {
    this.props.actions.getLanguagesAction();
    this.setState({ data: {}, showSaveModal: false });
  };
  handleChangeLanguages = checkedValues => {
    this.setState({ checkedValues });
  };
  handleSelectDefaultLanguage = value => {
    this.setState({ defaultLanguage: value });
  };
  submitDefaultLanguage = async () => {
    const { defaultLanguage } = this.state;
    await savesettingsApi({ name: "defaultLanguage", value: defaultLanguage });
    this.props.actions.getSettingsAction();
  };
  submitSelectLanguages = async () => {
    const { checkedValues } = this.state;
    const languages = _.get(this.props, "languages.data");
    await Promise.all(
      _.map(languages, async language => {
        const { id, enabled } = language;
        if (enabled && !_.includes(checkedValues, id)) {
          await updatelanguagesApi(id, { enabled: false });
        } else if (!enabled && _.includes(checkedValues, id)) {
          await updatelanguagesApi(id, { enabled: true });
        }
      })
    );
    this.props.actions.getLanguagesAction();
  };
  handleSubmit = async () => {
    await this.submitDefaultLanguage();
    await this.submitSelectLanguages();
    message.success("Update success.");
  };
  defaultLanguageBlock = (languages, defaultLanguage) => {
    return (
      <Select
        value={defaultLanguage}
        style={{ width: 120 }}
        onChange={this.handleSelectDefaultLanguage}
      >
        {_.compact(
          _.map(languages, language => {
            const { code, icon, name, enabled } = language;
            if (!enabled) return null;
            return (
              <Option key={code} value={code}>
                <span className={`flag-icon flag-icon-${icon}`} /> {name}
              </Option>
            );
          })
        )}
      </Select>
    );
  };
  activeLanguageBlock = (languages, checkedValues) => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    return (
      <Checkbox.Group
        value={checkedValues}
        onChange={this.handleChangeLanguages}
        style={{ width: "100%" }}
      >
        <div className="row">
          {_.map(languages, language => {
            const { id, icon, name } = language;
            return (
              <div key={id} className="col-md-3">
                <Checkbox value={id}>
                  <span className={`flag-icon flag-icon-${icon}`} /> {name}{" "}
                </Checkbox>
                <Icon
                  type="edit"
                  theme="outlined"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleEditLanguage(language)}
                />{" "}
                {authHasPermission(permissions, [`${permissionPrefix}.delete`]) && (
                  <Popconfirm
                    title="Are you sure delete this task?"
                    onConfirm={() => this.handleDeleteLanguage(id)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Icon type="close" theme="outlined" style={{ cursor: "pointer" }} />
                  </Popconfirm>
                )}
              </div>
            );
          })}
        </div>
      </Checkbox.Group>
    );
  };
  moduleContent = (languages, checkedValues, defaultLanguage) => {
    return (
      <ModuleBlock moduleName="setting-language" headerLabel="Language">
        <h5>Default Language</h5>
        {this.defaultLanguageBlock(languages, defaultLanguage)}
        <br />
        <br />
        <hr />
        <h5>Active Language</h5>
        {this.activeLanguageBlock(languages, checkedValues)}
      </ModuleBlock>
    );
  };
  modulePublish = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { submitting } = this.state;
    return (
      <ModulePublish
        submitting={submitting}
        onClick={this.handleSubmit}
        disabled={!authHasPermission(permissions, [`${permissionPrefix}.edit`])}
      />
    );
  };
  getHeaderActions = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const actions = [];
    actions.push(
      <Button
        type="primary"
        onClick={this.handleClickCreate}
        disabled={!authHasPermission(permissions, [`${permissionPrefix}.add`])}
      >
        Create
      </Button>
    );
    return actions;
  };
  render() {
    const { data, showSaveModal, checkedValues, defaultLanguage } = this.state;
    const languages = _.get(this.props, "languages.data");
    const headerActions = this.getHeaderActions();
    return (
      <div className="page-settings-languages list">
        <HeaderPage pageName="Setting Languages" actions={headerActions} />
        <div className="row">
          <div className="col-md-10">
            {this.moduleContent(languages, checkedValues, defaultLanguage)}
          </div>
          <div className="col-md-2">{this.modulePublish()}</div>
        </div>
        <Modal
          destroyOnClose
          visible={showSaveModal}
          title={_.isEmpty(data) ? "Create a new language" : "Update a language"}
          footer={null}
          onCancel={this.handleCancelModalCreate}
        >
          <SaveForm {...this.props} data={data} onSave={this.handleSaveLanguage} />
        </Modal>
      </div>
    );
  }
}
