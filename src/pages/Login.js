import React, { Component } from "react";
import { Form, Icon, Input, Button, Alert } from "antd";
import _ from "lodash";
import { Logo, BackgroundImage } from "../components";
import { loginAction } from "../actions/auth";
import { withRedux } from "../hoc";

const background = `${process.env.PUBLIC_URL}/assets/images/background.jpg`;

const FormItem = Form.Item;

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  loginAction
};

@Form.create()
@withRedux(mapStateToProps, actionToProps)
export default class PageLogin extends Component {
  state = {
    submitting: false,
    errorMessage: null
  };
  login = async (uid, password) => {
    if (this.state.submitting) return false;
    this.setState({ submitting: true, errorMessage: null });
    try {
      const loginResult = await this.props.actions.loginAction(uid, password);
      if (_.get(loginResult, "status") !== "success") {
        this.setState({ errorMessage: _.get(loginResult, "message", "Login Failed") });
      }
    } catch (error) {
      this.setState({ errorMessage: _.get(error, "message") });
    }
    this.setState({ submitting: false });
    return true;
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { uid, password } = values;
        this.login(uid, password);
      }
    });
  };
  render() {
    const { submitting, errorMessage } = this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="page-login">
        <BackgroundImage image={background} />
        <div className="login-form-block">
          <div className="logo">
            <Logo />
          </div>
          <div className="app-name">{process.env.REACT_APP_WEBSITE_NAME}</div>
          {errorMessage && (
            <Alert message={errorMessage} type="error" showIcon style={{ marginBottom: 15 }} />
          )}
          <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator("uid", {
                rules: [{ required: true, message: "Please input your username!" }]
              })(
                <Input
                  autoFocus
                  prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
                  placeholder="Username"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("password", {
                rules: [{ required: true, message: "Please input your Password!" }]
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
                  type="password"
                  placeholder="Password"
                />
              )}
            </FormItem>
            <FormItem>
              <Button
                type="primary"
                htmlType="submit"
                loading={submitting}
                className="login-form-button"
              >
                {submitting ? "Logging..." : "Log in"}
              </Button>
            </FormItem>
          </Form>
        </div>
      </div>
    );
  }
}
