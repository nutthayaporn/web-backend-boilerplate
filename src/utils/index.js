import _ from "lodash";

export const fetchJson = (url, options = {}) => {
  return fetch(url, options)
    .then(response => response.json())
    .catch(error => {
      console.log("error", error);
      return error;
    });
};

export const fetchApi = (url, method = "GET", data = {}, headers = {}) => {
  const options = {
    method,
    headers: {
      Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      ...headers
    }
  };
  if (method === "POST" || method === "PUT") {
    if (headers !== false) {
      options.headers = {
        "Content-Type": "application/json",
        ...options.headers
      };
    }
    options.body = data;
  }
  return fetchJson(`${process.env.REACT_APP_SERVICE}/${process.env.REACT_APP_API}/${url}`, options);
};

export const sendEmail = async ({ formId, data }) => {
  console.log("sendEmail data", data);
  const url = `${process.env.REACT_APP_FORVIZ_FORM_HOST}/forms/${formId}`;
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  };
  const result = await fetchJson(url, options).then(res => {
    console.log("sendEmail result", res);
    if (_.get(res, "data") === "success") {
      return true;
    }
    return false;
  });
  return result;
};

export const toObject = items => {
  return _.keyBy(items, "id");
};

export const sortCollection = items => {
  return _.orderBy(items, ["created_at"], ["desc"]);
};

export const stripHtml = html => {
  const temporalDivElement = document.createElement("div");
  temporalDivElement.innerHTML = html;
  return temporalDivElement.textContent || temporalDivElement.innerText || "";
};

export const JSONToCSVConvertor = (JSONData, ReportTitle, ShowLabel = true) => {
  //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
  const arrData = typeof JSONData !== "object" ? JSON.parse(JSONData) : JSONData;

  let CSV = "";
  //Set Report title in first row or line

  // CSV += ReportTitle + "\r\n\n";

  //This condition will generate the Label/Header
  if (ShowLabel) {
    let row = "";

    //This loop will extract the label from 1st index of on array
    for (const index in arrData[0]) {
      //Now convert each value to string and comma-seprated
      row += index + ",";
    }

    row = row.slice(0, -1);

    //append Label row with line break
    CSV += row + "\r\n";
  }

  //1st loop is to extract each row
  for (let i = 0; i < arrData.length; i++) {
    let row = "";

    //2nd loop will extract each column and convert it in string comma-seprated
    for (const index in arrData[i]) {
      row += '"' + _.get(arrData, `${i}.${index}`, "") + '",';
    }

    row.slice(0, row.length - 1);

    //add a line break after each row
    CSV += row + "\r\n";
  }

  if (CSV === "") {
    alert("Invalid data");
    return;
  }

  //Generate a file name
  //this will remove the blank-spaces from the title and replace it with an underscore
  const fileName = ReportTitle.replace(/ /g, "_");

  //Initialize file format you want csv or xls
  const uri = "data:text/csv;charset=utf-8," + escape(CSV);

  // Now the little tricky part.
  // you can use either>> window.open(uri);
  // but this will not work in some browsers
  // or you will not get the correct file extension

  //this trick will generate a temp <a /> tag
  const link = document.createElement("a");
  link.href = uri;

  //set the visibility hidden so it will not effect on your web-layout
  link.style = "visibility:hidden";
  link.download = fileName + ".csv";

  //this part will append the anchor tag and remove it after automatic click
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

export const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const authHasPermission = (authPermissions, permissions = []) => {
  permissions.push("all");
  if (_.isEmpty(authPermissions)) return false;
  const hasPermission = _.some(authPermissions, authPermission => {
    return _.includes(permissions, authPermission.slug);
  });
  return hasPermission;
};

export const convertSelectedFiles = value => {
  return _.isArray(value)
    ? _.isObject(_.get(value, "0"))
      ? _.map(value, v => _.get(v, "id"))
      : value
    : value;
};
