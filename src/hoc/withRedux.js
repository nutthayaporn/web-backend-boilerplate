import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash";

const initialActions = {};

export default (mapStateToProps, actions = {}) => Component => {
  const mapDispatchToProps = dispatch => ({
    dispatch,
    actions: bindActionCreators({ ...initialActions, ...actions }, dispatch)
  });
  @connect(
    mapStateToProps,
    mapDispatchToProps
  )
  class WithRedux extends React.Component {
    getDataFromObject = object => {
      const currentActive = _.get(this.props, "languages.currentActive");
      const defaultLanguage = _.get(this.props, "settings.data.defaultLanguage");
      let string = _.get(object, currentActive, _.get(object, defaultLanguage));
      if (!string) string = object[Object.keys(object)[0]];
      return string;
    };
    getDataWithLanguage = content => {
      let result = content;
      if (_.isObject(content)) result = this.getDataFromObject(content);
      return result;
    };
    render() {
      return <Component {...this.props} t={this.getDataWithLanguage} />;
    }
  }
  return WithRedux;
};
