import React from "react";
import _ from "lodash";
import { Login } from "../pages";
import { getAuthByTokenAction } from "../actions/auth";
import { withRedux } from "../hoc";

export default extendAction => Component => {
  const mapStateToProps = state => {
    return state;
  };
  const actionToProps = { ...extendAction, getAuthByTokenAction };
  @withRedux(mapStateToProps, actionToProps)
  class checkLogin extends React.Component {
    state = {
      loginChecked: false
    };
    componentDidMount() {
      this.checkToken();
    }
    checkToken = async () => {
      const token = window.localStorage.getItem("token");
      if (token) {
        try {
          await this.props.actions.getAuthByTokenAction(token);
        } catch (error) {
          console.log("checkToken error", error);
        }
      }
      this.setState({ loginChecked: true });
    };
    render() {
      if (!this.state.loginChecked) return <div>loading...</div>;
      const isLogin = _.get(this.props, "auth.isLogin", false);
      if (!isLogin) return <Login />;
      return <Component {...this.props} />;
    }
  }
  return checkLogin;
};
