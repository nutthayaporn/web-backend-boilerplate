import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { renderRoutes } from "react-router-config";
import configureStore from "./store/configureStore";
import routes from "./config/routes";
import { RouterMiddleware } from "./components";
// import registerServiceWorker from "./registerServiceWorker";

window.matchMedia =
  window.matchMedia ||
  function() {
    return {
      matches: false,
      addListener: function() {},
      removeListener: function() {}
    };
  };

const store = configureStore();

ReactDOM.render(
  <Provider store={store} key="provider">
    <BrowserRouter basename={`${process.env.PUBLIC_URL}`}>
      <RouterMiddleware routes={routes}>{renderRoutes(routes)}</RouterMiddleware>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
// registerServiceWorker();
