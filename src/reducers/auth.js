import * as ActionType from "../actions/actionType";

const initialState = {
  data: {},
  isLogin: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionType.RECEIVE_AUTH:
      return {
        ...state,
        data: action.data
      };
    case ActionType.IS_LOGIN:
      return {
        ...state,
        isLogin: action.isLogin
      };
    default:
      return state;
  }
};
