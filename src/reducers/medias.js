import * as ActionType from "../actions/actionType";

const initialState = {
  data: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionType.RECEIVE_MEDIAS:
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};
