import * as ActionType from "../actions/actionType";

const initialState = {
  data: {
    defaultLanguage: null
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionType.RECEIVE_SETTINGS:
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};
