import _ from "lodash";
import * as ActionType from "./actionType";
import { getlanguagesApi } from "../services/languages";
import { getmediasApi } from "../services/medias";
import { getsettingsApi } from "../services/settings";

const initApp = () => {
  return async dispatch => {
    const listApi = [];
    listApi.push(
      getlanguagesApi().then(data => {
        dispatch({
          type: ActionType.RECEIVE_LANGUAGES,
          data
        });
      })
    );
    listApi.push(
      getmediasApi().then(data => {
        dispatch({
          type: ActionType.RECEIVE_MEDIAS,
          data
        });
      })
    );
    listApi.push(
      getsettingsApi().then(data => {
        dispatch({
          type: ActionType.RECEIVE_SETTINGS,
          data
        });
        const defaultLanguage = _.get(data, "defaultLanguage");
        dispatch({
          type: ActionType.SWITCH_LANGUAGE,
          language: defaultLanguage
        });
      })
    );
    try {
      await Promise.all(listApi);
    } catch (err) {
      console.error("initApp err", err);
    }
  };
};

export { initApp };
