import * as ActionType from "../actions/actionType";
import { getmediasApi } from "../services/medias";

const getMediasAction = () => {
  return dispatch => {
    return getmediasApi().then(data => {
      dispatch({
        type: ActionType.RECEIVE_MEDIAS,
        data
      });
    });
  };
};

export { getMediasAction };
