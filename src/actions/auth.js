import _ from "lodash";
import * as ActionType from "./actionType";
import { loginApi, getAuthByTokenApi } from "../services/auth";
import { authHasPermission } from "../utils";

const setLogin = (dispatch, data) => {
  dispatch({
    type: ActionType.RECEIVE_AUTH,
    data
  });
  dispatch({
    type: ActionType.IS_LOGIN,
    isLogin: true
  });
};

const loginAction = (uid, password) => {
  return dispatch => {
    return loginApi(uid, password).then(tokenResult => {
      const tokenData = _.get(tokenResult, "data");
      if (!tokenData) return tokenResult;
      const { token } = tokenData;
      window.localStorage.setItem("token", token);
      return getAuthByTokenApi(token).then(userResult => {
        const userData = _.get(userResult, "data");
        if (!userData) return userResult;
        if (!authHasPermission(_.get(userData, "permissions")))
          return { status: "error", message: "You are not allowed" };
        setLogin(dispatch, userData);
        return userData;
      });
    });
  };
};

const logoutAction = () => {
  return dispatch => {
    window.localStorage.removeItem("token");
    dispatch({
      type: ActionType.IS_LOGIN,
      isLogin: false
    });
  };
};

const getAuthByTokenAction = token => {
  return dispatch => {
    return getAuthByTokenApi(token).then(userResult => {
      const userData = _.get(userResult, "data");
      if (!userData) return userResult;
      if (!authHasPermission(_.get(userData, "permissions")))
        return { status: "error", message: "You are not allowed" };
      setLogin(dispatch, userData);
      return userData;
    });
  };
};

export { loginAction, logoutAction, getAuthByTokenAction };
