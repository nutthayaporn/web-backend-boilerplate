import * as ActionType from "../actions/actionType";
import { getlanguagesApi } from "../services/languages";

const switchLanguage = language => ({
  type: ActionType.SWITCH_LANGUAGE,
  language
});

const getLanguagesAction = () => {
  return dispatch => {
    return getlanguagesApi().then(data => {
      dispatch({
        type: ActionType.RECEIVE_LANGUAGES,
        data
      });
    });
  };
};

export { switchLanguage, getLanguagesAction };
