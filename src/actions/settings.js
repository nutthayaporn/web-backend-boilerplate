import _ from "lodash";
import * as ActionType from "../actions/actionType";
import { getsettingsApi } from "../services/settings";

const getSettingsAction = () => {
  return dispatch => {
    return getsettingsApi().then(data => {
      dispatch({
        type: ActionType.RECEIVE_SETTINGS,
        data
      });
      const defaultLanguage = _.get(data, "defaultLanguage");
      dispatch({
        type: ActionType.SWITCH_LANGUAGE,
        language: defaultLanguage
      });
      return {
        data,
        defaultLanguage
      };
    });
  };
};

export { getSettingsAction };
