import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Modal, Upload, Icon, message, Pagination } from "antd";
import _ from "lodash";
import { BackgroundImage, DragAndDrop } from "../components";
import { withRedux } from "../hoc";
import { getMediasAction } from "../actions/medias";
import { sortCollection, convertSelectedFiles } from "../utils";

const Dragger = Upload.Dragger;

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  getMediasAction
};

@withRedux(mapStateToProps, actionToProps)
export default class Media extends Component {
  static propTypes = {
    type: PropTypes.oneOf(["file", "image", "gallery"])
  };
  static defaultProps = {
    type: "file"
  };
  state = {
    lastValue: null,
    selectedFiles: [],
    selectedFilesTmp: [],
    uploading: false,
    showMediaModal: false,
    currentPage: 1,
    pageSize: 10
  };
  static getDerivedStateFromProps(props, state) {
    if (!_.isEqual(props.value, state.lastValue)) {
      let selectedFiles = convertSelectedFiles(props.value);
      if (!_.isArray(selectedFiles)) selectedFiles = [selectedFiles];
      return {
        lastValue: props.value,
        selectedFiles,
        selectedFilesTmp: selectedFiles
      };
    }
    return null;
  }
  handleClickAddMedia = () => {
    this.setState({ showMediaModal: !this.state.showMediaModal });
  };
  handleCancelMediaModal = () => {
    this.setState({ selectedFilesTmp: [], showMediaModal: false });
  };
  onChange = () => {
    const { type } = this.props;
    if (type === "gallery") {
      this.props.onChange(_.compact(this.state.selectedFiles));
    } else {
      this.props.onChange(_.get(this.state, "selectedFiles.0", null));
    }
  };
  handleOk = () => {
    this.setState(
      {
        selectedFiles: this.state.selectedFilesTmp,
        showMediaModal: false
      },
      () => {
        this.onChange();
      }
    );
  };
  uploadButton = () => {
    return (
      <div>
        <Icon type={this.state.uploading ? "loading" : "plus"} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
  };
  beforeUpload = file => {
    const { type } = this.props;
    const acceptFile = this.getAcceptFile();
    if (!acceptFile) {
      if (type === "gallery" || type === "image") {
        message.error("You can only upload Image or Video file!");
      } else if (type === "file") {
        message.error("You can only upload Pdf file!");
      }
    }
    return true;
  };
  handleChange = info => {
    if (info.file.status === "uploading") {
      this.setState({ uploading: true });
      return;
    }
    if (info.file.status === "done") {
      this.props.actions.getMediasAction();
      this.setState({ uploading: false });
    }
  };
  handleSelectMedia = id => {
    const { type } = this.props;
    const { selectedFilesTmp } = this.state;
    let selectedFilesTmpNew = [...selectedFilesTmp];
    if (_.includes(selectedFilesTmp, id)) {
      // selectedFilesTmpNew = _.filter(selectedFilesTmp, item => item !== id);
    } else {
      if (type === "gallery") {
        selectedFilesTmpNew.push(id);
      } else {
        selectedFilesTmpNew = [id];
      }
    }
    this.setState({ selectedFilesTmp: selectedFilesTmpNew });
  };
  handleDeselectMedia = id => {
    const { selectedFilesTmp } = this.state;
    let selectedFilesTmpNew = [...selectedFilesTmp];
    if (_.includes(selectedFilesTmp, id)) {
      selectedFilesTmpNew = _.filter(selectedFilesTmp, item => item !== id);
    } else {
      // selectedFilesTmpNew.push(id);
    }
    this.setState({ selectedFilesTmp: selectedFilesTmpNew });
  };
  handleRemoveMedia = id => {
    const { selectedFiles } = this.state;
    let selectedFilesNew = [...selectedFiles];
    if (_.includes(selectedFiles, id)) {
      selectedFilesNew = _.filter(selectedFiles, item => item !== id);
    } else {
      // selectedFilesNew.push(id);
    }
    this.setState({ selectedFiles: selectedFilesNew }, () => {
      this.onChange();
    });
  };
  getAcceptFile = () => {
    const { type } = this.props;
    if (type === "gallery" || type === "image") {
      return "image/*, video/*";
    } else if (type === "file") {
      return "application/pdf";
    }
    return "";
  };
  renderMediaByType = media => {
    const { file_name, file_path, type } = media;
    if (type === "image") {
      return <BackgroundImage image={file_path} />;
    } else if (type === "application" || type === "video") {
      return (
        <div className="type-file">
          <div className="icon-file">
            <Icon type="file" theme="outlined" />
          </div>
          <div className="file-name">{file_name}</div>
        </div>
      );
    }
  };
  getMedias = () => {
    const { type } = this.props;
    let medias = sortCollection(_.get(this.props, "medias.data"));
    if (type === "gallery" || type === "image") {
      medias = _.filter(medias, media => _.includes(["image", "video"], media.type));
    } else if (type === "file") {
      medias = _.filter(medias, { type: "application" });
    }
    return medias;
  };
  handleChangePagination = (page, pageSize) => {
    this.setState({ currentPage: page, pageSize });
  };
  handleShowSizeChange = (current, pageSize) => {
    this.setState({ currentPage: current, pageSize });
  };
  handleReorderMedia = items => {
    const ids = _.map(items, item => item.id);
    this.setState({ selectedFiles: ids, selectedFilesTmp: ids }, () => {
      this.onChange();
    });
  };
  render() {
    const { showMediaModal, selectedFiles, selectedFilesTmp, currentPage, pageSize } = this.state;
    const { type } = this.props;
    const medias = this.getMedias();
    const mediaCount = _.size(medias);
    const mediasPagination = _.slice(medias, (currentPage - 1) * pageSize, currentPage * pageSize);
    return (
      <div className="media-component">
        <Button type="primary" onClick={this.handleClickAddMedia}>
          {`Add ${_.startCase(type)}`}
        </Button>
        <div className="media-items">
          {!_.isEmpty(selectedFiles) && !_.isEmpty(medias) && (
            <DragAndDrop
              items={_.map(selectedFiles, selectedFile => {
                const media = _.find(medias, { id: selectedFile });
                const id = _.get(media, "id");
                const file_path = _.get(media, "file_path");
                return {
                  id,
                  file_path,
                  media
                };
              })}
              uiRenderer={item => (
                <div className="item">
                  <div className="check-block" onClick={() => this.handleRemoveMedia(item.id)}>
                    <Icon type="close" theme="outlined" />
                  </div>
                  <a href={item.file_path} target="_blank">
                    {this.renderMediaByType(item.media)}
                  </a>
                </div>
              )}
              onChange={this.handleReorderMedia}
            />
          )}
        </div>
        <Modal
          className="media-modal"
          title="Media Library"
          visible={showMediaModal}
          okText="Select"
          cancelText={null}
          onOk={this.handleOk}
          onCancel={this.handleCancelMediaModal}
          destroyOnClose
          width="100%"
          footer={[
            <div key="left" className="left">
              <Pagination
                showSizeChanger
                onChange={this.handleChangePagination}
                onShowSizeChange={this.handleShowSizeChange}
                current={currentPage}
                total={mediaCount}
                showTotal={total => `Total ${total} items`}
                showQuickJumper
              />
            </div>,
            <div key="right" className="right">
              <Button key="back" onClick={this.handleCancelMediaModal}>
                Cancel
              </Button>
              <Button key="submit" type="primary" onClick={this.handleOk}>
                Select
              </Button>
            </div>
          ]}
        >
          <div className="media-items">
            <div className="item item-upload">
              <Dragger
                accept={this.getAcceptFile()}
                name="file"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                multiple
                action={`${process.env.REACT_APP_SERVICE}/${process.env.REACT_APP_API}/medias`}
                beforeUpload={this.beforeUpload}
                onChange={this.handleChange}
              >
                {this.uploadButton()}
              </Dragger>
            </div>
            {_.map(mediasPagination, media => {
              const { id } = media;
              const active = _.includes(selectedFilesTmp, id);
              const activeClassName = active ? "active" : "";
              return (
                <div className={`item ${activeClassName}`} key={id}>
                  {active && (
                    <div className="check-block" onClick={() => this.handleDeselectMedia(id)}>
                      <Icon className="check" type="check" theme="outlined" />
                      <Icon className="minus" type="minus" theme="outlined" />
                    </div>
                  )}
                  <div
                    onClick={() => this.handleSelectMedia(id)}
                    style={{ width: "100%", height: "100%" }}
                  >
                    {this.renderMediaByType(media)}
                  </div>
                </div>
              );
            })}
          </div>
        </Modal>
      </div>
    );
  }
}
