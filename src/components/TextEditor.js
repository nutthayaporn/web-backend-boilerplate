import React, { Component } from "react";
import _ from "lodash";
import { Editor } from "react-draft-wysiwyg";
import { createmediasApi } from "../services/medias";

export default class TextEditor extends Component {
  uploadImageCallBack = file => {
    return new Promise(async (resolve, reject) => {
      try {
        const uploadResult = await createmediasApi(file);
        if (_.get(uploadResult, "status") === "success") {
          resolve({
            data: {
              ..._.get(uploadResult, "data"),
              link: _.get(uploadResult, "data.file_path")
            }
          });
        } else {
          reject(_.get(uploadResult, "message"));
        }
      } catch (error) {
        reject(error);
      }
    });
  };
  render() {
    return (
      <Editor
        {...this.props}
        toolbar={{
          inline: { inDropdown: true },
          list: { inDropdown: true },
          textAlign: { inDropdown: true },
          link: { inDropdown: true },
          history: { inDropdown: true },
          image: {
            uploadCallback: this.uploadImageCallBack,
            previewImage: true,
            alt: { present: true, mandatory: false }
          }
        }}
      />
    );
  }
}
