import React, { PureComponent } from "react";
import PropTypes from "prop-types";

const noImageAvailable = `${process.env.PUBLIC_URL}/assets/images/No_Image_Available.jpg`;

export default class Image extends PureComponent {
  state = {
    src: noImageAvailable
  };
  componentWillMount() {
    this.setState({ src: this.props.src || noImageAvailable });
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.src !== nextProps.src) {
      this.setState({ src: nextProps.src || noImageAvailable });
    }
  }
  static propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string
  };
  static defaultProps = {
    src: noImageAvailable,
    alt: process.env.REACT_APP_WEBSITE_NAME
  };
  handleError = () => {
    this.setState({ src: noImageAvailable });
  };
  render() {
    const { src } = this.state;
    const { alt } = this.props;
    return <img {...this.props} src={src} alt={alt} onError={this.handleError} />;
  }
}
