import React, { PureComponent } from "react";
import { Collapse } from "antd";

const Panel = Collapse.Panel;

export default class ModuleBlock extends PureComponent {
  render() {
    const { children, moduleName, headerLabel } = this.props;
    return (
      <div className={`module-block ${moduleName}`}>
        <Collapse defaultActiveKey={["1"]}>
          <Panel header={headerLabel} key="1">
            {children}
          </Panel>
        </Collapse>
      </div>
    );
  }
}
