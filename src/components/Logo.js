import React, { PureComponent } from "react";

const logo = `${process.env.PUBLIC_URL}/assets/images/logo.svg`;

export default class Logo extends PureComponent {
  render() {
    return <img src={logo} alt={process.env.REACT_APP_WEBSITE_NAME} />;
  }
}
