import React, { PureComponent } from "react";
import { Button } from "antd";
import { ModuleBlock } from "../components";

export default class ModulePublish extends PureComponent {
  render() {
    const { submitting, disabled } = this.props;
    return (
      <ModuleBlock moduleName="publish" headerLabel="Publish">
        <Button
          type="primary"
          loading={submitting}
          block
          onClick={this.props.onClick}
          disabled={disabled}
        >
          {submitting ? "Saving..." : "Save"}
        </Button>
      </ModuleBlock>
    );
  }
}
