import List from "./List";
import Save from "./Save";
import Page from "./Page";

export default { List, Save, Page };
