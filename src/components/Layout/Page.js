import React, { Component } from "react";
import { Form, message, Affix } from "antd";
import _ from "lodash";
import { HeaderPage, ModulePublish, InputToFormItem, ModuleBlock } from "../../components";
import { withRedux } from "../../hoc";
import { getMediasAction } from "../../actions/medias";
import { fetchApi, sortCollection, authHasPermission } from "../../utils";

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  getMediasAction
};

@Form.create()
@withRedux(mapStateToProps, actionToProps)
export default class LayoutPage extends Component {
  state = {
    loaded: false,
    submitting: false,
    data: {},
    otherResources: {}
  };
  componentDidMount() {
    this.init();
  }
  init = () => {
    this.getData();
    this.fetchOtherResources();
    this.props.actions.getMediasAction();
    setTimeout(() => {
      const firstElement = document.querySelector(".ant-form-item-children");
      if (_.get(firstElement, "childNodes")) firstElement.childNodes[0].focus();
    }, 200);
  };
  getData = async () => {
    const { resource } = this.props;
    const apiData = await fetchApi(resource).then(res => _.get(res, "data"));
    this.setState({
      loaded: true,
      data: apiData
    });
  };
  fetchOtherResources = async () => {
    const otherResources = _.get(this.props, "otherResources");
    if (_.isEmpty(otherResources)) return;
    for (let index = 0; index < otherResources.length; index++) {
      const otherResource = otherResources[index];
      const getData = await fetchApi(`${otherResource}`)
        .then(res => _.get(res, "data"))
        .then(sortCollection);
      this.setState({
        otherResources: {
          ...this.state.otherResources,
          [otherResource]: getData
        }
      });
    }
  };
  submit = async data => {
    const { resource } = this.props;
    if (this.state.submitting) return false;
    this.setState({ submitting: true });
    const saveResult = await fetchApi(resource, "POST", JSON.stringify(data));
    if (_.get(saveResult, "status") === "success") {
      message.success("Save success.");
    } else {
      message.error(_.get(saveResult, "message", "Save failed."));
    }
    this.setState({ submitting: false });
    return true;
  };
  checkNotTranslate = currentFormItem => {
    const notTranslateThisField =
      _.get(currentFormItem, "notTranslate") === true ||
      _.get(currentFormItem, "relation") === true;
    return notTranslateThisField;
  };
  convertData = (formItems, values, oldData) => {
    const currentLanguage = _.get(this.props, "languages.currentActive");
    const datas = _.reduce(
      formItems,
      (result, formItem) => {
        let datasResult = null;
        const { field, inputType } = formItem;
        const currentValue = values[field];
        const oldValue = _.get(oldData, field);
        if (inputType === "repeat") {
          datasResult = {
            ...result,
            [field]: _.compact(
              _.map(currentValue, (v, k) => {
                if (_.isEmpty(v)) return null;
                return _.reduce(
                  v,
                  (vResult, vValue, vKey) => {
                    const currentFormItem = _.find(formItem.formItems, { field: vKey });
                    if (_.get(currentFormItem, "inputType") === "repeat") {
                      return {
                        ...vResult,
                        [vKey]: _.map(vValue, (vValue2, vKey2) => {
                          const next = _.get(oldValue, `${k}.${vKey}.${vKey2}`);
                          return this.convertData(
                            _.get(currentFormItem, "formItems"),
                            vValue2,
                            next
                          );
                        })
                      };
                    }
                    if (this.checkNotTranslate(currentFormItem)) {
                      return {
                        ...vResult,
                        [vKey]: vValue
                      };
                    } else {
                      return {
                        ...vResult,
                        [vKey]: {
                          ..._.get(oldValue, `${k}.${vKey}`),
                          [currentLanguage]: vValue
                        }
                      };
                    }
                  },
                  {}
                );
              })
            )
          };
        } else if (inputType === "cascader") {
          datasResult = {
            ...result,
            [field]: _.last(currentValue)
          };
        } else if (this.checkNotTranslate(formItem)) {
          datasResult = {
            ...result,
            [field]: currentValue
          };
        } else {
          datasResult = {
            ...result,
            [field]: {
              ...oldValue,
              [currentLanguage]: currentValue
            }
          };
        }
        return datasResult;
      },
      {}
    );
    return datas;
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { formItems } = this.props;
        const notTranslate = _.get(this.props, "notTranslate");
        const oldData = _.get(this.state, "data");
        const datas = notTranslate ? values : this.convertData(formItems, values, oldData);
        this.submit(datas);
      }
    });
  };
  moduleContent = () => {
    const { data, otherResources } = this.state;
    const { t, form, pageName, formItems } = this.props;
    return (
      <ModuleBlock
        moduleName="content"
        headerLabel={pageName}
        key={this.state.loaded ? "update" : "create"}
      >
        <Form onSubmit={this.handleSubmit}>
          <InputToFormItem
            t={t}
            form={form}
            formItems={formItems}
            data={data}
            otherResources={otherResources}
          />
        </Form>
      </ModuleBlock>
    );
  };
  modulePublish = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { submitting } = this.state;
    return (
      <ModulePublish
        submitting={submitting}
        onClick={this.handleSubmit}
        disabled={!authHasPermission(permissions, [`${permissionPrefix}.save`])}
      />
    );
  };
  render() {
    const { pageName } = this.props;
    return (
      <div className={`page-${_.kebabCase(pageName)}`}>
        <HeaderPage pageName={pageName} />
        <div className="row">
          <div className="col-md-10">{this.moduleContent()}</div>
          <div className="col-md-2">
            <Affix offsetTop={84}>{this.modulePublish()}</Affix>
          </div>
        </div>
      </div>
    );
  }
}
