import React, { Component } from "react";
import { Form, message, Button, Icon, Affix, Popconfirm, Tooltip } from "antd";
import _ from "lodash";
import { HeaderPage, ModulePublish, InputToFormItem, ModuleBlock } from "../../components";
import { withRedux } from "../../hoc";
import { getMediasAction } from "../../actions/medias";
import { fetchApi, sortCollection, authHasPermission } from "../../utils";

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  getMediasAction
};

@Form.create()
@withRedux(mapStateToProps, actionToProps)
export default class LayoutSave extends Component {
  state = {
    loaded: false,
    submitting: false,
    data: {},
    saveType: "create",
    otherResources: {},
    list: {}
  };
  componentDidMount() {
    this.init();
  }
  init = () => {
    this.getDataList();
    this.fetchById();
    this.fetchOtherResources();
    this.props.actions.getMediasAction();
    setTimeout(() => {
      const firstElement = document.querySelector(".ant-form-item-children");
      if (_.get(firstElement, "childNodes")) firstElement.childNodes[0].focus();
    }, 200);
  };
  getDataList = async () => {
    const { resource, sort } = this.props;
    let apiData = await fetchApi(resource)
      .then(res => _.get(res, "data"))
      .then(res => (sort ? _.orderBy(res, ["sort"], ["asc"]) : sortCollection(res)));
    this.setState({
      list: apiData
    });
  };
  fetchById = async () => {
    const { id } = this.props.match.params;
    if (id !== "0") {
      const { resource } = this.props;
      const getData = await fetchApi(`${resource}/${id}`);
      if (_.get(getData, "status") === "success") {
        this.setState({
          data: _.get(getData, "data"),
          saveType: "update"
        });
      }
    }
    this.setState({ loaded: true });
  };
  fetchOtherResources = async () => {
    const otherResources = _.get(this.props, "otherResources");
    if (_.isEmpty(otherResources)) return;
    for (let index = 0; index < otherResources.length; index++) {
      const otherResource = otherResources[index];
      const getData = await fetchApi(`${otherResource}`)
        .then(res => _.get(res, "data"))
        .then(sortCollection);
      this.setState({
        otherResources: {
          ...this.state.otherResources,
          [otherResource]: getData
        }
      });
    }
  };
  create = async data => {
    const { resource, redirectPath } = this.props;
    const created = await fetchApi(resource, "POST", JSON.stringify(data));
    this.setState({ submitting: false });
    if (_.get(created, "status") === "success") {
      message.success("Create success.");
      this.props.history.push(redirectPath);
      return true;
    } else {
      message.error(_.get(created, "message", "Create failed."));
      return false;
    }
  };
  update = async data => {
    const { resource, redirectPath } = this.props;
    const updated = await fetchApi(
      `${resource}/${this.state.data.id}`,
      "PUT",
      JSON.stringify(data)
    );
    this.setState({ submitting: false });
    if (_.get(updated, "status") === "success") {
      message.success("Update success.");
      this.props.history.push(redirectPath);
      return true;
    } else {
      message.error(_.get(updated, "message", "Update failed."));
      return false;
    }
  };
  submit = async data => {
    if (this.state.submitting) return false;
    this.setState({ submitting: true });
    const { saveType } = this.state;
    if (saveType === "update") {
      await this.update(data);
    } else {
      await this.create(data);
    }
    return true;
  };
  checkNotTranslate = currentFormItem => {
    const notTranslateThisField =
      _.get(currentFormItem, "notTranslate") === true ||
      _.get(currentFormItem, "relation") === true;
    return notTranslateThisField;
  };
  convertData = (formItems, values, oldData) => {
    const currentLanguage = _.get(this.props, "languages.currentActive");
    const datas = _.reduce(
      formItems,
      (result, formItem) => {
        let datasResult = null;
        const { field, inputType } = formItem;
        const currentValue = values[field];
        const oldValue = _.get(oldData, field);
        if (inputType === "repeat") {
          datasResult = {
            ...result,
            [field]: _.compact(
              _.map(currentValue, (v, k) => {
                if (_.isEmpty(v)) return null;
                return _.reduce(
                  v,
                  (vResult, vValue, vKey) => {
                    const currentFormItem = _.find(formItem.formItems, { field: vKey });
                    if (_.get(currentFormItem, "inputType") === "repeat") {
                      return {
                        ...vResult,
                        [vKey]: _.map(vValue, (vValue2, vKey2) => {
                          const next = _.get(oldValue, `${k}.${vKey}.${vKey2}`);
                          return this.convertData(
                            _.get(currentFormItem, "formItems"),
                            vValue2,
                            next
                          );
                        })
                      };
                    }
                    if (this.checkNotTranslate(currentFormItem)) {
                      return {
                        ...vResult,
                        [vKey]: vValue
                      };
                    } else {
                      return {
                        ...vResult,
                        [vKey]: {
                          ..._.get(oldValue, `${k}.${vKey}`),
                          [currentLanguage]: vValue
                        }
                      };
                    }
                  },
                  {}
                );
              })
            )
          };
        } else if (inputType === "cascader") {
          datasResult = {
            ...result,
            [field]: _.last(currentValue)
          };
        } else if (this.checkNotTranslate(formItem)) {
          datasResult = {
            ...result,
            [field]: currentValue
          };
        } else {
          datasResult = {
            ...result,
            [field]: {
              ...oldValue,
              [currentLanguage]: currentValue
            }
          };
        }
        return datasResult;
      },
      {}
    );
    return datas;
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { formItems } = this.props;
        const notTranslate = _.get(this.props, "notTranslate");
        const oldData = _.get(this.state, "data");
        const datas = notTranslate ? values : this.convertData(formItems, values, oldData);
        this.submit(datas);
      }
    });
  };
  moduleContent = () => {
    const { data, otherResources, list, saveType } = this.state;
    const { t, form, pageName, formItems } = this.props;
    return (
      <ModuleBlock
        moduleName="content"
        headerLabel={pageName}
        key={this.state.loaded ? "update" : "create"}
      >
        <Form onSubmit={this.handleSubmit}>
          <InputToFormItem
            t={t}
            form={form}
            formItems={formItems}
            data={data}
            otherResources={otherResources}
            list={list}
            saveType={saveType}
            id={_.toInteger(this.props.match.params.id)}
          />
        </Form>
      </ModuleBlock>
    );
  };
  modulePublish = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { submitting, saveType } = this.state;
    const noPermission =
      saveType === "create"
        ? !authHasPermission(permissions, [`${permissionPrefix}.add`])
        : !authHasPermission(permissions, [`${permissionPrefix}.edit`]);
    return (
      <ModulePublish submitting={submitting} onClick={this.handleSubmit} disabled={noPermission} />
    );
  };
  handleClickDelete = async id => {
    const { resource, redirectPath } = this.props;
    const deleteResult = await fetchApi(`${resource}/${id}`, "DELETE");
    if (_.get(deleteResult, "status") === "success") {
      message.success("Delete success.");
      this.props.history.push(redirectPath);
    } else {
      message.error(_.get(deleteResult, "message", "Delete failed."));
    }
  };
  getHeaderActions = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const actions = [];
    const { id } = this.props.match.params;
    // actions.push(
    //   <Button type="primary" onClick={() => window.print()}>
    //     <Icon type="printer" theme="outlined" />
    //   </Button>
    // );
    if (id !== "0") {
      if (authHasPermission(permissions, [`${permissionPrefix}.delete`])) {
        actions.push(
          <Popconfirm
            title="Are you sure delete this task?"
            onConfirm={() => this.handleClickDelete(id)}
            okText="Yes"
            cancelText="No"
          >
            <Tooltip title="Delete">
              <Button type="primary">
                <Icon type="delete" />
              </Button>
            </Tooltip>
          </Popconfirm>
        );
      }
    }
    return actions;
  };
  render() {
    const { pageName, redirectPath } = this.props;
    const { saveType } = this.state;
    const headerActions = this.getHeaderActions();
    return (
      <div className={`page-${_.kebabCase(pageName)} save`}>
        <HeaderPage
          pageName={`${saveType === "update" ? "Update" : "Create"} ${pageName}`}
          breadcrumbs={[
            {
              path: redirectPath,
              label: pageName
            },
            {
              path: "",
              label: saveType === "update" ? "Update" : "Create"
            }
          ]}
          actions={headerActions}
        />
        <div className="row">
          <div className="col-md-10">{this.moduleContent()}</div>
          <div className="col-md-2">
            <Affix offsetTop={84}>{this.modulePublish()}</Affix>
          </div>
        </div>
      </div>
    );
  }
}
