import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Input,
  Table,
  Button,
  Icon,
  Popconfirm,
  message,
  Tooltip,
  Select,
  DatePicker,
  Upload,
  Tag
} from "antd";
import _ from "lodash";
import moment from "moment";
import Fuse from "fuse.js";
import draftToHtml from "draftjs-to-html";
import { withRedux } from "../../hoc";
import { HeaderPage, DragSortingTable } from "../../components";
import { getMediasAction } from "../../actions/medias";
import { importApi } from "../../services/import";
import {
  fetchApi,
  sortCollection,
  stripHtml,
  JSONToCSVConvertor,
  authHasPermission
} from "../../utils";

const Search = Input.Search;
const Option = Select.Option;
const { RangePicker } = DatePicker;

const mapStateToProps = state => {
  return state;
};

const actionToProps = {
  getMediasAction
};

@withRedux(mapStateToProps, actionToProps)
export default class LayoutList extends Component {
  state = {
    data: [],
    trashData: [],
    otherResources: {},
    searchValue: null,
    selectedRowKeys: [],
    bulkAction: null,
    filterTime: {
      startTime: null,
      endTime: null
    },
    filters: {},
    importing: false,
    loading: false,
    displayStatus: "all"
  };
  componentDidMount() {
    this.init();
  }
  init = () => {
    this.getData();
    this.fetchOtherResources();
    this.props.actions.getMediasAction();
  };
  getData = async () => {
    this.setState({ loading: true });
    const { resource, sort, order } = this.props;
    // get data
    const apiData = await fetchApi(resource)
      .then(res => _.get(res, "data"))
      .then(res => {
        if (sort) {
          return _.orderBy(res, ["sort", "created_at"], ["asc", "desc"]);
        } else if (!_.isEmpty(order)) {
          return _.orderBy(res, order[0], order[1]);
        } else {
          return sortCollection(res);
        }
      });
    // get trash data
    const trashData = await fetchApi(`trashes/table/${resource}`)
      .then(res => _.get(res, "data"))
      .then(sortCollection);
    this.setState({
      data: apiData,
      trashData,
      loading: false
    });
  };
  fetchOtherResources = async () => {
    const otherResources = _.get(this.props, "otherResources");
    if (_.isEmpty(otherResources)) return;
    for (let index = 0; index < otherResources.length; index++) {
      const otherResource = otherResources[index];
      const getData = await fetchApi(`${otherResource}`)
        .then(res => _.get(res, "data"))
        .then(sortCollection);
      this.setState({
        otherResources: {
          ...this.state.otherResources,
          [otherResource]: getData
        }
      });
    }
  };
  handleDelete = async id => {
    const { resource } = this.props;
    const deleteResult = await fetchApi(`${resource}/${id}`, "DELETE");
    if (_.get(deleteResult, "status") === "success") {
      await this.getData();
      message.success("Delete success.");
    } else {
      message.error(_.get(deleteResult, "message", "Delete failed."));
    }
  };
  handleSearch = searchValue => {
    if (this.state.searchValue !== searchValue) {
      this.setState({ searchValue });
    }
  };
  filterData = datas => {
    const languages = this.props.languages.data;
    const { filterKey } = this.props;
    const notTranslate = _.get(this.props, "notTranslate");
    const { searchValue, filterTime, filters } = this.state;
    let result = datas;
    if (filterTime.startTime && filterTime.endTime) {
      result = _.filter(datas, data =>
        moment(data.created_at).isBetween(filterTime.startTime, filterTime.endTime)
      );
    }
    if (!_.isEmpty(filters)) {
      _.map(filters, (value, resource) => {
        if (value === undefined || value === null) return;
        const filter = _.find(this.props.filters, { resource });
        const findKey = _.get(filter, "key");
        result = _.filter(result, v => _.get(v, findKey) === value);
      });
    }
    if (searchValue) {
      let keys = [];
      if (notTranslate) {
        keys = filterKey;
      } else {
        _.map(filterKey, value => {
          _.map(languages, language => {
            keys.push(`${value}.${language.code}`);
          });
        });
      }
      const options = {
        shouldSort: true,
        threshold: 0.1,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys
      };
      const datasArray = _.map(datas, data => data);
      const fuse = new Fuse(datasArray, options);
      result = fuse.search(searchValue);
    }
    return result;
  };
  translateData = (data, fields, mapFieldTitle) => {
    const { t } = this.props;
    let result = {};
    _.map(fields, value => {
      const { key, title } = value;
      _.set(result, mapFieldTitle ? title : key, t(_.get(data, key)));
    });
    return result;
  };
  getFieldsFromData = (fields, mapFieldTitle = false) => {
    const { displayStatus } = this.state;
    const data = this.filterData(
      displayStatus === "trash" ? this.state.trashData : this.state.data
    );
    let keyIndex = 1;
    const result = _.map(data, v => {
      return {
        key: keyIndex++,
        id: _.get(v, "id"),
        ...this.translateData(v, fields, mapFieldTitle),
        created_at: _.get(v, "created_at"),
        updated_at: _.get(v, "updated_at")
      };
    });
    return result;
  };
  tableRenderColumn = (text, record, type, typeData) => {
    let html = _.toString(text);
    if (type === "link") {
      html = (
        <a href={text} target="_blank">
          Link
        </a>
      );
    } else if (type === "wysiwyg") {
      html = stripHtml(draftToHtml(text));
    } else if (type === "tag") {
      html = <Tag color={_.get(typeData, text)}>{text}</Tag>;
    } else if (type === "datetime") {
      html = moment(text).format("YYYY-MM-DD HH:mm:ss");
    } else if (type === "label") {
      html = _.get(typeData, text);
    }
    return html;
  };
  handleRestore = async trash_id => {
    const restoreResult = await fetchApi(`trashes/restore/${trash_id}`);
    if (_.get(restoreResult, "status") === "success") {
      await this.getData();
      message.success("Restore success.");
    } else {
      message.error(_.get(restoreResult, "message", "Restore failed."));
    }
  };
  handleDeletePermanently = async trash_id => {
    const deleteResult = await fetchApi(`trashes/${trash_id}`, "DELETE");
    if (_.get(deleteResult, "status") === "success") {
      await this.getData();
      message.success("Delete success.");
    } else {
      message.error(_.get(deleteResult, "message", "Delete failed."));
    }
  };
  getTableColumn = () => {
    const { trashData, displayStatus } = this.state;
    const userRoles = _.get(this.props, "auth.data.roles");
    const isAdministrator = _.some(userRoles, v => {
      return _.get(v, "slug") === "administrator";
    });
    let canEditDeleteIds = [];
    if (_.get(this.props, "resource") === "roles") {
      canEditDeleteIds = [1, 2, 3];
    } else if (_.get(this.props, "resource") === "users") {
      canEditDeleteIds = [1];
    }
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { tableDisplay } = this.props;
    const { path } = this.props.route;
    const tableColumn = [];
    tableColumn.push({
      title: "#",
      dataIndex: "key",
      key: "key",
      sorter: (a, b) => _.toInteger(a.key) - _.toInteger(b.key),
      render: (text, record) => {
        return text;
      }
    });
    _.map(tableDisplay, value => {
      const { key, title, type, typeData } = value;
      tableColumn.push({
        title,
        dataIndex: key,
        key,
        sorter: (a, b) => a[key].localeCompare(b[key]),
        render: (text, record) => this.tableRenderColumn(text, record, type, typeData)
      });
    });
    tableColumn.push({
      title: "Date",
      dataIndex: "created_at",
      key: "created_at",
      sorter: (a, b) => a.created_at.localeCompare(b.created_at)
    });
    tableColumn.push({
      title: "Actions",
      dataIndex: "id",
      key: "id",
      render: id => {
        const conditionEditDelete =
          isAdministrator || _.isEmpty(canEditDeleteIds) || !_.includes(canEditDeleteIds, id);
        const currentTrashData = _.find(trashData, { id });
        const trash_id = _.get(currentTrashData, "trash_id");
        return (
          <ul className="actions-btn">
            {conditionEditDelete && displayStatus === "all" && (
              <li>
                <Link to={`${path}/save/${id}`}>
                  <Tooltip title="Edit">
                    <Icon type="edit" />
                  </Tooltip>
                </Link>
              </li>
            )}
            {conditionEditDelete &&
              displayStatus === "all" &&
              (authHasPermission(permissions, [`${permissionPrefix}.delete`]) && (
                <li>
                  <Popconfirm
                    title="Are you sure delete this task?"
                    onConfirm={() => this.handleDelete(id)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Tooltip title="Delete">
                      <Icon type="delete" />
                    </Tooltip>
                  </Popconfirm>
                </li>
              ))}
            {conditionEditDelete &&
              displayStatus === "trash" &&
              (authHasPermission(permissions, [`${permissionPrefix}.delete`]) && (
                <li>
                  <Popconfirm
                    title="Are you sure restore this task?"
                    onConfirm={() => this.handleRestore(trash_id)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Tooltip title="Restore">
                      <Icon type="rollback" />
                    </Tooltip>
                  </Popconfirm>
                </li>
              ))}
            {conditionEditDelete &&
              displayStatus === "trash" &&
              (authHasPermission(permissions, [`${permissionPrefix}.delete`]) && (
                <li>
                  <Popconfirm
                    title="Are you sure delete this task?"
                    onConfirm={() => this.handleDeletePermanently(trash_id)}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Tooltip title="Delete Permanently">
                      <Icon type="delete" />
                    </Tooltip>
                  </Popconfirm>
                </li>
              ))}
          </ul>
        );
      }
    });
    return tableColumn;
  };
  getTableData = () => {
    return this.getFieldsFromData(this.props.tableDisplay);
  };
  setDataToTable = () => {
    return {
      tableColumn: this.getTableColumn(),
      tableData: this.getTableData()
    };
  };
  importButton = () => {
    const props = {
      accept: ".csv",
      name: "file",
      showUploadList: false,
      beforeUpload: file => {
        this.setState({ importing: true });
        const acceptFile = ["application/vnd.ms-excel"];
        const accept = _.includes(acceptFile, file.type);
        if (!accept) {
          message.error("You can only upload csv file!");
          this.setState({ importing: false });
          return false;
        }
        importApi(file, this.props.resource).then(async res => {
          this.setState({ importing: false });
          if (_.get(res, "status") === "success") {
            await this.getData();
            this.props.actions.getMediasAction();
            message.success(`${file.name} file uploaded successfully`);
          } else {
            message.error(`${file.name} file upload failed.`);
          }
        });
        return false;
      }
    };
    return (
      <Upload {...props}>
        <Button disabled={this.state.importing}>
          <Icon type={this.state.importing ? "loading" : "upload"} /> Import
        </Button>
      </Upload>
    );
  };
  getHeaderActions = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { path } = this.props.route;
    const actions = [];
    // actions.push(this.importButton());
    actions.push(
      <Link to={`${path}/save/0`}>
        <Button
          type="primary"
          disabled={!authHasPermission(permissions, [`${permissionPrefix}.add`])}
        >
          Create
        </Button>
      </Link>
    );
    return actions;
  };
  onRowSelection = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };
  getRowSelection = () => {
    const { selectedRowKeys } = this.state;
    return {
      selectedRowKeys,
      onChange: this.onRowSelection
    };
  };
  handleChangeBulkActions = value => {
    this.setState({ bulkAction: value });
  };
  deleteBulk = async () => {
    const { resource } = this.props;
    const { selectedRowKeys, data } = this.state;
    for (let index = 0; index < selectedRowKeys.length; index++) {
      const selectedRowKey = selectedRowKeys[index];
      const currentData = data[selectedRowKey - 1];
      await fetchApi(`${resource}/${currentData.id}`, "DELETE");
    }
    await this.getData();
    this.setState({ selectedRowKeys: [], bulkAction: null });
    message.success("Delete success.");
  };
  restoreBulk = async () => {
    const { selectedRowKeys, trashData } = this.state;
    for (let index = 0; index < selectedRowKeys.length; index++) {
      const selectedRowKey = selectedRowKeys[index];
      const currentData = trashData[selectedRowKey - 1];
      await fetchApi(`trashes/restore/${currentData.trash_id}`);
    }
    await this.getData();
    this.setState({ selectedRowKeys: [], bulkAction: null });
    message.success("Restore success.");
  };
  deletePermanentlyBulk = async () => {
    const { selectedRowKeys, trashData } = this.state;
    for (let index = 0; index < selectedRowKeys.length; index++) {
      const selectedRowKey = selectedRowKeys[index];
      const currentData = trashData[selectedRowKey - 1];
      await fetchApi(`trashes/${currentData.trash_id}`, "DELETE");
    }
    await this.getData();
    this.setState({ selectedRowKeys: [], bulkAction: null });
    message.success("Delete success.");
  };
  handleApplyBulkActions = () => {
    const { selectedRowKeys, bulkAction } = this.state;
    if (_.isEmpty(selectedRowKeys)) return;
    if (_.isEmpty(bulkAction)) return;
    if (bulkAction === "delete") this.deleteBulk();
    if (bulkAction === "restore") this.restoreBulk();
    if (bulkAction === "delete_permanently") this.deletePermanentlyBulk();
  };
  handleChangeDateFilter = value => {
    this.setState({
      filterTime: {
        startTime: _.get(value, "0", null),
        endTime: _.get(value, "1", null)
      }
    });
  };
  handleExportExcel = () => {
    const { displayStatus } = this.state;
    const { pageName, exportFields } = this.props;
    let data = this.filterData(displayStatus === "trash" ? this.state.trashData : this.state.data);
    const mapFieldTitle = true;
    if (exportFields) data = this.getFieldsFromData(exportFields, mapFieldTitle);
    JSONToCSVConvertor(data, pageName);
  };
  handleFilters = (resource, value) => {
    this.setState({
      filters: {
        ...this.state.filters,
        [resource]: value
      }
    });
  };
  renderActionsBar = () => {
    const permissions = _.get(this.props, "auth.data.permissions");
    const permissionPrefix = _.get(this.props, "route.permissionPrefix");
    const { t } = this.props;
    const filters = _.get(this.props, "filters");
    const { otherResources, displayStatus } = this.state;
    const options = [{ value: null, label: "Bulk Actions" }];
    if (displayStatus === "all") {
      if (authHasPermission(permissions, [`${permissionPrefix}.delete`])) {
        options.push({ value: "delete", label: "Delete" });
      }
    }
    if (displayStatus === "trash") {
      if (authHasPermission(permissions, [`${permissionPrefix}.delete`])) {
        options.push({ value: "restore", label: "Restore" });
        options.push({ value: "delete_permanently", label: "Delete Permanently" });
      }
    }
    return (
      <div className="actions-bar">
        <div className="left">
          <ul>
            <li>
              <Select
                placeholder="Bulk Actions"
                style={{ width: 120 }}
                onChange={this.handleChangeBulkActions}
                value={this.state.bulkAction}
              >
                {_.map(options, (option, index) => {
                  return (
                    <Option key={index} value={option.value}>
                      {option.label}
                    </Option>
                  );
                })}
              </Select>{" "}
              <Button type="primary" onClick={this.handleApplyBulkActions}>
                Apply
              </Button>
            </li>
            <li>
              <RangePicker
                ranges={{
                  Today: [moment().startOf("day"), moment().endOf("day")],
                  "This Month": [moment(), moment().endOf("month")]
                }}
                showTime={{
                  format: "HH:mm",
                  defaultValue: [moment("00:00", "HH:mm"), moment("23:59", "HH:mm")]
                }}
                format="YYYY-MM-DD HH:mm"
                placeholder={["Start Time", "End Time"]}
                onChange={this.handleChangeDateFilter}
              />{" "}
            </li>
            {!_.isEmpty(filters) &&
              _.map(filters, (filter, index) => {
                const { resource, type, mapResource, placeholder, width, items } = filter;
                const mapResourceId = _.get(mapResource, "id");
                const mapResourceLabel = _.get(mapResource, "label");
                const datas = _.get(otherResources, resource, items);
                return (
                  <li key={index}>
                    {type === "select" && (
                      <Select
                        showSearch
                        allowClear
                        placeholder={placeholder}
                        filterOption={(input, option) =>
                          _.toLower(option.props.children).indexOf(_.toLower(input)) >= 0
                        }
                        onChange={value => this.handleFilters(resource, value)}
                        style={{ minWidth: width || 120 }}
                      >
                        {_.map(datas, (data, index) => {
                          return (
                            <Option key={index} value={_.get(data, mapResourceId)}>
                              {t(_.get(data, mapResourceLabel))}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </li>
                );
              })}
            <li>
              <Button type="primary" icon="download" onClick={this.handleExportExcel}>
                Excel
              </Button>
            </li>
          </ul>
        </div>
        <div className="right">
          <ul>
            <li>
              <Search autoFocus placeholder="Search" enterButton onSearch={this.handleSearch} />
            </li>
          </ul>
        </div>
      </div>
    );
  };
  update = async (id, data) => {
    const { resource } = this.props;
    const updated = await fetchApi(`${resource}/${id}`, "PUT", JSON.stringify(data));
    return updated;
  };
  handleMove = async value => {
    const { data, dragIndex, hoverIndex } = value;
    let startIndex = dragIndex;
    let endIndex = hoverIndex;
    if (dragIndex > hoverIndex) {
      startIndex = hoverIndex;
      endIndex = dragIndex;
    }
    for (let index = startIndex; index <= endIndex; index++) {
      const element = data[index];
      await this.update(element.id, { sort: index });
    }
  };
  handleClickDisplayStatus = displayStatus => {
    if (this.state.displayStatus === displayStatus) return;
    this.setState({ displayStatus, bulkAction: null, selectedRowKeys: [] });
  };
  renderFilterList = () => {
    const { data, trashData, displayStatus } = this.state;
    return (
      <ul className="filter-list-bar">
        <li
          className={displayStatus === "all" ? "active" : ""}
          onClick={() => this.handleClickDisplayStatus("all")}
        >
          All ({_.size(data)})
        </li>
        <li
          className={displayStatus === "trash" ? "active" : ""}
          onClick={() => this.handleClickDisplayStatus("trash")}
        >
          Trash ({_.size(trashData)})
        </li>
      </ul>
    );
  };
  render() {
    const { loading } = this.state;
    const { pageName, sort } = this.props;
    const { tableColumn, tableData } = this.setDataToTable();
    const headerActions = this.getHeaderActions();
    const rowSelection = this.getRowSelection();
    const tableProps = {
      rowSelection,
      columns: tableColumn,
      dataSource: tableData,
      pagination: {
        showSizeChanger: true,
        pageSizeOptions: ["10", "20", "30", "40", "50", "100"],
        showTotal: total => `Total ${total} items`,
        showQuickJumper: true
      },
      onMove: this.handleMove,
      size: "small",
      loading
    };
    return (
      <div className={`page-${_.kebabCase(pageName)} list`}>
        <HeaderPage pageName={pageName} actions={headerActions} />
        {this.renderFilterList()}
        {this.renderActionsBar()}
        {sort ? <DragSortingTable {...tableProps} /> : <Table {...tableProps} />}
      </div>
    );
  }
}
