import React, { PureComponent } from "react";
import { Breadcrumb } from "antd";
import { Link } from "react-router-dom";
import _ from "lodash";

export default class HeaderPage extends PureComponent {
  render() {
    const { pageName, breadcrumbs, actions } = this.props;
    return (
      <div className="header-page">
        <Breadcrumb>
          {_.map(breadcrumbs, (breadcrumb, key) => {
            const { path, label } = breadcrumb;
            let breadcrumbHtml = label;
            if (path !== "") breadcrumbHtml = <Link to={path}>{label}</Link>;
            return <Breadcrumb.Item key={key}>{breadcrumbHtml}</Breadcrumb.Item>;
          })}
        </Breadcrumb>
        <div className="row align-items-center">
          <div className="col-6 left">
            <h1>{pageName}</h1>
          </div>
          <div className="col-6 right">
            <ul>
              {_.map(actions, (action, key) => {
                return <li key={key}>{action}</li>;
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
