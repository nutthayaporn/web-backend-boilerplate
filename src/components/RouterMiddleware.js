import { Component } from "react";
import _ from "lodash";
import { withRouter } from "react-router-dom";
import { withRedux } from "../hoc";
import { authHasPermission } from "../utils";

const mapStateToProps = state => {
  return state;
};

const actionToProps = {};

@withRouter
@withRedux(mapStateToProps, actionToProps)
export default class RouterMiddleware extends Component {
  state = {
    hasPermission: true
  };
  componentDidMount() {
    this.init();
  }
  componentDidUpdate(prevProps) {
    if (!_.isEqual(this.props.location, prevProps.location)) {
      this.init();
    } else if (_.get(this.props, "auth.data.permissions")) {
      this.checkPermission();
    }
  }
  findRouteDataByRoute = value => {
    const routes = _.get(this.props, "routes.0.routes");
    return _.find(routes, route => {
      const tokens = _.split(route.path, "/");
      const regexTokens = _.map(tokens, token => {
        if (_.includes(token, ":")) {
          return "(.)";
        }
        return token;
      });
      const parseString = _.join(_.compact(regexTokens), "/");
      const regex = new RegExp(`^/${parseString}$`, "ig");
      const test = regex.test(value);
      return test;
    });
  };
  checkPermission = () => {
    const authPermissions = _.get(this.props, "auth.data.permissions");
    if (authPermissions) {
      const { pathname } = this.props.location;
      const routeData = this.findRouteDataByRoute(pathname);
      const permissions = _.get(routeData, "permissions");
      if (permissions) {
        const hasPermission = authHasPermission(authPermissions, permissions);
        if (this.state.hasPermission !== hasPermission) {
          this.setState({ hasPermission });
          if (!hasPermission) this.props.history.push("/");
        }
      }
    }
  };
  init = () => {
    window.scrollTo(0, 0);
    this.checkPermission();
  };
  render() {
    return this.props.children;
  }
}
