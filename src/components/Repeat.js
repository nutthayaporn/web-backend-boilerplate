import React, { PureComponent } from "react";
import { Button, Icon } from "antd";
import _ from "lodash";
import styled from "styled-components";
import { InputToFormItem } from "../components";

const RepeatComponentStyle = styled.div`
  .label {
    position: relative;
    color: rgba(0, 0, 0, 0.85);
    &:after {
      content: ":";
      margin: 0 8px 0 2px;
      position: relative;
      top: -0.5px;
    }
  }
  .row-value {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    border: 1px solid #d9d9d9;
    padding: 10px;
    margin-bottom: 10px;
    position: relative;
    .ant-form-item {
      flex: 1 1 50%;
      padding: 0 15px;
    }
  }
  .add-block {
    text-align: right;
  }
  .remove-block {
    position: absolute;
    top: 5px;
    right: 5px;
    i {
      cursor: pointer;
      font-size: 16px;
    }
  }
`;

const defaultRows = [0];

export default class Repeat extends PureComponent {
  state = {
    rows: defaultRows,
    prevData: null
  };
  static getDerivedStateFromProps(props, state) {
    const currentData = _.get(props, `data.${props.field}`);
    if (!_.isEqual(currentData, state.prevData)) {
      let rows = defaultRows;
      if (!_.isEmpty(currentData)) rows = _.map(currentData, (v, i) => i);
      return {
        rows,
        prevData: currentData
      };
    }
    return null;
  }
  fieldName = () => {
    return `${this.props.field}_keys`;
  };
  handleAddRow = () => {
    const { rows } = this.state;
    const nextKeys = rows.concat(_.last(rows) + 1);
    this.setState({ rows: nextKeys });
  };
  handleRemoveRow = k => {
    const { rows } = this.state;
    if (rows.length === 1) {
      return;
    }
    const nextKeys = rows.filter(key => key !== k);
    this.setState({ rows: nextKeys });
  };
  setFieldNameFormItems = index => {
    const formItems = _.get(this.props, "formItems");
    return _.map(formItems, formItem => {
      const { field } = formItem;
      return {
        ...formItem,
        field: `${this.props.field}[${index}]${field}`
      };
    });
  };
  render() {
    const { rows } = this.state;
    const { label } = this.props;
    return (
      <RepeatComponentStyle className="repeat-component">
        <div className="label">{label}</div>
        <div className="value-block">
          {_.map(rows, key => {
            const formItems = this.setFieldNameFormItems(key);
            return (
              <div className={`row-value row-${key}`} key={key}>
                <InputToFormItem {...this.props} formItems={formItems} row={key} />
                {rows.length > 1 && (
                  <div className="remove-block">
                    <Icon type="close" theme="outlined" onClick={() => this.handleRemoveRow(key)} />
                  </div>
                )}
              </div>
            );
          })}
        </div>
        <div className="add-block">
          <Button type="primary" onClick={this.handleAddRow}>
            Add Row
          </Button>
        </div>
      </RepeatComponentStyle>
    );
  }
}
