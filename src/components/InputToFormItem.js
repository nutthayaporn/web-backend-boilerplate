import React, { Component } from "react";
import { Form, Input, InputNumber, Checkbox, Select, DatePicker, Cascader } from "antd";
import _ from "lodash";
import moment from "moment";
import { TextEditor, Repeat, MediaPopup } from ".";
import { convertSelectedFiles } from "../utils";

const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
const { RangePicker } = DatePicker;

export default class InputToFormItem extends Component {
  static defaultProps = {
    saveType: "update"
  };
  render() {
    let { list } = this.props;
    const { t, form, formItems, data, otherResources, saveType, id } = this.props;
    const { getFieldDecorator } = form;
    return (
      <React.Fragment>
        {_.map(formItems, (formItem, key) => {
          const {
            field,
            label,
            inputType,
            fieldDecorator,
            resource,
            mapResource,
            defaultValue,
            fieldTime,
            unique,
            notTranslate,
            extra,
            disabled,
            relation
          } = formItem;
          const mapResourceId = _.get(mapResource, "id");
          const mapResourceLabel = _.get(mapResource, "label");
          const mapResourceParent = _.get(mapResource, "parent");
          const value = _.get(data, field, defaultValue);
          let valuePropName = "value";
          let inputHtml = <Input />;
          let initialValue =
            notTranslate === true || relation === true || _.get(this.props, "notTranslate") === true
              ? value
              : t(value);
          let fieldValue = field;
          if (_.includes(["text", "password"], inputType)) {
            inputHtml = (
              <Input
                type={inputType}
                disabled={disabled}
                onChange={e => {
                  if (this.props.formItems[key].onChange) {
                    return this.props.formItems[key].onChange(this.props, e.target.value);
                  }
                }}
              />
            );
          } else if (inputType === "wysiwyg") {
            initialValue = _.isEmpty(initialValue) ? null : initialValue;
            initialValue = _.isObject(initialValue) ? initialValue : null;
            valuePropName = "initialContentState";
            inputHtml = <TextEditor />;
          } else if (inputType === "number") {
            inputHtml = <InputNumber disabled={disabled} />;
          } else if (inputType === "checkbox") {
            valuePropName = "checked";
            inputHtml = <Checkbox />;
          } else if (_.includes(["file", "image", "gallery"], inputType)) {
            if (initialValue) initialValue = convertSelectedFiles(initialValue);
            inputHtml = <MediaPopup type={inputType} />;
          } else if (inputType === "textarea") {
            inputHtml = <TextArea rows={3} />;
          } else if (inputType === "select") {
            const mode = _.get(formItem, "mode", "default");
            if (mode === "multiple" || mode === "tags") {
              if (mapResourceId) {
                initialValue = _.map(initialValue, v => _.get(v, mapResourceId));
              }
            }
            let selectData = resource ? _.get(otherResources, resource) : _.get(formItem, "items");
            if (unique) {
              if (saveType === "update") {
                list = _.filter(list, l => l.id !== id);
              }
              selectData = _.filter(selectData, v => {
                return !_.includes(_.map(list, l => _.get(l, field)), v.id);
              });
            }
            const getKey = value => _.get(value, mapResourceId);
            const getValue = value => t(_.get(value, mapResourceLabel));
            const selectValue = value => {
              return resource ? getKey(value) : _.get(value, "key");
            };
            const selectLabel = value => {
              return resource ? getValue(value) : _.get(value, "label");
            };
            inputHtml = (
              <Select
                mode={mode}
                showSearch
                placeholder={label}
                filterOption={(input, option) =>
                  _.toLower(option.props.children).indexOf(_.toLower(input)) >= 0
                }
                disabled={disabled}
              >
                {_.map(selectData, v => {
                  const value = selectValue(v);
                  const label = selectLabel(v);
                  return (
                    <Option key={value} value={value}>
                      {label}
                    </Option>
                  );
                })}
              </Select>
            );
          } else if (inputType === "repeat") {
            return <Repeat key={key} {...this.props} {...formItem} />;
          } else if (inputType === "range-time-picker") {
            const startField = _.get(fieldTime, "start");
            const endField = _.get(fieldTime, "end");
            initialValue = [
              t(_.get(data, startField)) ? moment(t(_.get(data, startField))) : null,
              t(_.get(data, endField)) ? moment(t(_.get(data, endField))) : null
            ];
            inputHtml = (
              <RangePicker
                showTime={{
                  format: "YYYY-MM-DD HH:mm",
                  defaultValue: [moment("00:00:00", "HH:mm:ss"), moment("00:00:00", "HH:mm:ss")]
                }}
                format="YYYY-MM-DD HH:mm"
                onChange={value => {
                  if (this.props.formItems[key].onChange) {
                    return this.props.formItems[key].onChange(value);
                  }
                }}
              />
            );
          } else if (inputType === "cascader") {
            const newInitialValue = [initialValue];
            const selectData = _.get(otherResources, resource);
            const findDataById = id => {
              return _.find(selectData, { [mapResourceId]: id });
            };
            const setInitialValue = id => {
              const current = findDataById(id);
              const parent_id = _.get(current, mapResourceParent);
              if (parent_id && parent_id !== 0) {
                newInitialValue.unshift(parent_id);
                setInitialValue(parent_id);
              }
            };
            setInitialValue(initialValue);
            initialValue = newInitialValue;
            const findChildren = v => {
              const options = {
                value: _.get(v, mapResourceId),
                label: t(_.get(v, mapResourceLabel))
              };
              const parent = _.filter(selectData, { [mapResourceParent]: _.get(v, mapResourceId) });
              if (!_.isEmpty(parent)) options.children = _.map(parent, findChildren);
              return options;
            };
            const firstLevel = _.filter(selectData, { [mapResourceParent]: 0 });
            const options = _.map(firstLevel, findChildren);
            inputHtml = <Cascader options={options} placeholder="" />;
          } else if (inputType === "plaintext") {
            inputHtml = <span className="ant-form-text">{initialValue}</span>;
          }
          return (
            <FormItem key={key} label={label} extra={extra}>
              {getFieldDecorator(fieldValue, {
                ...fieldDecorator,
                initialValue,
                valuePropName
              })(inputHtml)}
            </FormItem>
          );
        })}
      </React.Fragment>
    );
  }
}
