import React, { PureComponent } from "react";
import styled from "styled-components";

const noImageAvailable = `${process.env.PUBLIC_URL}/assets/images/No_Image_Available.jpg`;

const BackgroundImageStyle = styled.div`
  width: 100%;
  height: 100%;
  background-image: url('${props => props.image}');
  background-size: ${props => props.size};
  background-repeat: ${props => props.repeat};
  background-position: ${props => props.position};
`;

export default class BackgroundImage extends PureComponent {
  static defaultProps = {
    image: noImageAvailable,
    size: "cover",
    repeat: "no-repeat",
    position: "center"
  };
  render() {
    return (
      <BackgroundImageStyle
        className="background-image-component"
        {...this.props}
        image={this.props.image || noImageAvailable}
      >
        {this.props.children}
      </BackgroundImageStyle>
    );
  }
}
